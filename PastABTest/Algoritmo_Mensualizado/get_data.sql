
create table TESTS.AlgMen as (
SELECT distinct p.distinct_id,
case when  properties like  '%PP_Mensualizado_EN%' or properties like '%PP_Mensualizado_ESP%' or properties like  '%PP_Mensualizado_PT%' and properties like '%"Exp_Type":"A"%'  then 'A'
  when  properties like  '%PP_Mensualizado_EN%' or properties like '%PP_Mensualizado_ESP%' or properties like  '%PP_Mensualizado_PT%' and properties like '%"Exp_Type":"B"%'  then 'B'
  when  properties like  '%PP_Mensualizado_EN%' or properties like '%PP_Mensualizado_ESP%' or properties like  '%PP_Mensualizado_PT%' and properties like '%"Exp_Type":"C"%'  then 'C'
end as Mensualizado,
case  when properties like '%AB_Algorithm%' and properties like '%"Exp_Type":"A"%' then 'Alg_A'
  when properties like '%AB_Algorithm%' and properties like '%"Exp_Type":"B"%' then 'Alg_B'
end as Algoritmo,
u.id as user_id,
if(ue.premium_conversion_date is not null , 'Premium', 'Freemium') as status,
u.created_at as inicio_user,
ue.premium_conversion_date,
case when cha.payment_source in ( 'Email', 'INDAP', 'IAM', 'PUSH', 'PromoCode') then 'Promo'
     when cha.payment_source is null then 'NoCompra'
     else 'Producto' end as source,
cha.usd_amount_paid
FROM `celtic-music-240111.mixpanel.event` as ev
inner join `celtic-music-240111.mixpanel.people` as p
on p.distinct_id = ev.distinct_id
inner join aws_kinedu_app.users as u
on p.email= u.email
inner join (select  * from aws_kinedu_app.user_extra_data where _fivetran_deleted =false) as ue
on ue.user_id = u.id
left join aws_kinedu_app.charges as cha
on cha.id = ue.premium_conversion_charge
where ev.name ='AB_TEST'
and  properties like  '%PP_Mensualizado_EN%' or properties like '%PP_Mensualizado_ESP%' or properties like  '%PP_Mensualizado_PT%' or properties like '%AB_Algorithm%'
and ev.time >= '2019-08-19'
and p.email is not null)

#########################################
create table TESTS.AlgMenEngagement as (
select distinct ev.distinct_id , ev.time
from mixpanel.event as ev
where ev.name='ActivityView'
and ev.time>='2019-08-19')
##########################################
Luego baje esta tabla como AlgMen.csv y use es script AlgMen.r

#####################################################
SELECT beta.*
FROM `celtic-music-240111.TESTS.AlgMenEngagement` as alpha
left join TESTS.AlgMen as beta
on alpha.distinct_id = beta.distinct_id
where beta.user_id is not null
 y baje el resultado como AlgMenEngagement
 #####################################################

 

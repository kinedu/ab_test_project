create table TESTS.Men as (
SELECT distinct
case when  properties like  '%PP_Mensualizado_EN%' or properties like '%PP_Mensualizado_ESP%' or properties like  '%PP_Mensualizado_PT%' and properties like '%"Exp_Type":"A"%'  then 'A'
  when  properties like  '%PP_Mensualizado_EN%' or properties like '%PP_Mensualizado_ESP%' or properties like  '%PP_Mensualizado_PT%' and properties like '%"Exp_Type":"B"%'  then 'B'
  when  properties like  '%PP_Mensualizado_EN%' or properties like '%PP_Mensualizado_ESP%' or properties like  '%PP_Mensualizado_PT%' and properties like '%"Exp_Type":"C"%'  then 'C'
end as Mensualizado,
u.id as user_id,
if(ue.premium_conversion_date is not null , 'Premium', 'Freemium') as status,
u.created_at as inicio_user,
ue.premium_conversion_date
FROM `celtic-music-240111.mixpanel.event` as ev
inner join `celtic-music-240111.mixpanel.people` as p
on p.distinct_id = ev.distinct_id
inner join aws_kinedu_app.users as u
on p.email= u.email
inner join (select  * from aws_kinedu_app.user_extra_data where _fivetran_deleted =false) as ue
on ue.user_id = u.id
where ev.name ='AB_TEST'
and  properties like  '%PP_Mensualizado_EN%' or properties like '%PP_Mensualizado_ESP%' or properties like  '%PP_Mensualizado_PT%'
and ev.time >= '2019-08-19'
and p.email is not null)




create table TESTS.Alg as (
SELECT distinct
case  when properties like '%AB_Algorithm%' and properties like '%"Exp_Type":"A"%' then 'Alg_A'
  when properties like '%AB_Algorithm%' and properties like '%"Exp_Type":"B"%' then 'Alg_B'
end as Algoritmo,
u.id as user_id,
if(ue.premium_conversion_date is not null , 'Premium', 'Freemium') as status,
u.created_at as inicio_user,
ue.premium_conversion_date
FROM `celtic-music-240111.mixpanel.event` as ev
inner join `celtic-music-240111.mixpanel.people` as p
on p.distinct_id = ev.distinct_id
inner join aws_kinedu_app.users as u
on p.email= u.email
inner join (select  * from aws_kinedu_app.user_extra_data where _fivetran_deleted =false) as ue
on ue.user_id = u.id

where ev.name ='AB_TEST'
and  properties like '%AB_Algorithm%'
and ev.time >= '2019-08-19'
and p.email is not null)


create table TESTS.AlgMenUsers as (
SELECT distinct alg.Algoritmo,
if( Alg.status is not null ,Alg.status, Men.status) as status ,
if(alg.inicio_user is not null, alg.inicio_user, Men.inicio_user) as inicio_user,
if(alg.premium_conversion_date is not null ,alg.premium_conversion_date, Men.premium_conversion_date) as premium_conversion_date ,
Men.Mensualizado,
if ( Men.user_id is not null , Men.user_id,  Alg.user_id) as user_id
FROM `celtic-music-240111.TESTS.Alg` as alg
full outer join TESTS.Men as Men
on alg.user_id = Men.user_id
order by Algoritmo, Mensualizado)



create table TESTS.AlgMen2 as (
SELECT distinct  alpha.*,
case when cha.payment_source in ( 'Email', 'INDAP', 'IAM', 'PUSH', 'PromoCode') then 'Promo'
     when cha.payment_source is null then 'NoCompra'
     else 'Producto' end as source,
cha.usd_amount_paid
FROM `celtic-music-240111.TESTS.AlgMenUsers` as alpha
inner join aws_kinedu_app.user_extra_data as ue
on ue.user_id = alpha.user_id
left join ( select * from aws_kinedu_app.charges where status='paid' and sale_id is not null ) as cha
on cha.id = ue.premium_conversion_charge
order by user_id)
################
Luego baje esta tabla como AlgMen.csv y use es script AlgMen2.r


#######################
create table TESTS.AlgMenEngagement2 as (

select distinct ev.distinct_id , ev.time, p.user_id
from mixpanel.event as ev
left join mixpanel.people as p
on p.distinct_id = ev.distinct_id
where ev.name='ActivityView'
and ev.time>='2019-08-19')
##########################

#####################################################
SELECT beta.*
FROM `celtic-music-240111.TESTS.AlgMenEngagement` as alpha
left join TESTS.AlgMen as beta
on alpha.distinct_id = beta.distinct_id
where beta.user_id is not null
 y baje el resultado como AlgMenEngagement2
 #####################################################

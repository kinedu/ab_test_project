create table TESTS.TestMensualizado as
select alpha.* 
from 
(SELECT distinct distinct_id , time ,  
case when properties like '%"Exp_Type":"B"%' then 'B'
     when properties like '%"Exp_Type":"A"%'  then 'A'
     when properties like '%"Exp_Type":"C"%' then 'C'
     end as type, 
case when properties like '%PP_Mensualizado_EN%' then 'EN_test'
     when properties like '%PP_Mensualizado_ESP%' then 'ES_test'
     when properties like '%PP_Mensualizado_PT%' then 'PT_test'
     end as test
FROM mixpanel.event ev
where properties like '%PP_Mensualizado_EN%' or properties like '%PP_Mensualizado_ESP%' or properties like '%PP_Mensualizado_PT%'
and ev.name ='AB_TEST'
order by distinct_id ) as alpha




# el resultado anterior lo guarde 

create table TESTS.TestMensualizadoUsers as 
SELECT distinct   alpha.type, alpha.test, gamma.id as user_id
FROM `celtic-music-240111.TESTS.TestMensualizado` as alpha
inner join mixpanel.people as beta
on alpha.distinct_id = beta.distinct_id
inner join aws_kinedu_app.users as gamma 
on gamma.email= beta.email
order by user_id

# ya nomas hay que ponerles los pagos y sku y directo a R 

seLECT distinct  alpha.*, 
if(ue.premium_conversion_date is not null , 'Premium', 'Freemium') as conversion, sales.usd_amount, 
case when payment_source in ( 'Email', 'INDAP', 'IAM', 'PUSH', 'PromoCode') then 'PromoMKT'
     else 'Prod'
     end payment
FROM `celtic-music-240111.TESTS.TestMensualizadoUsers` as alpha
left join (select * from aws_kinedu_app.user_extra_data where _fivetran_deleted=false) as ue
on ue.user_id= alpha.user_id
inner join ( select * from aws_kinedu_app.sales where false=false) as sales
on ue.user_id = sales.user_id
inner join ( select * from aws_kinedu_app.charges where false=false) as charges
on charges.sale_id = sales.id
where
ue.premium_conversion_date >= '2019-08-19'
and ue.created_at >= '2019-08-19'
#and sales.
order by user_id

# esta tabla la llame TestMensualizadoSales y la exporte a R
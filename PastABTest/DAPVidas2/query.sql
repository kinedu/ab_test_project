SELECT distinct u.id as user_id, u.created_at as user_created, u.email,
u.locale as idioma_back,
case when properties like '%"Exp_Type":"B"%' then 'B'
     when properties like '%"Exp_Type":"A"%'  then 'A'
     end as type
FROM
mixpanel.event ev
  left join mixpanel.people p on ev.distinct_id = p.distinct_id
  join aws_kinedu_app.users u on u.email = p.email
where ev.name ='AB_TEST'
and properties like '%DAP_Vidas(Jul26)%'
and u.created_at >= '2019-07-26'
order by u.id


SELECT *
FROM `celtic-music-240111.TESTS.users_dapvidas2`
where type='B'





SELECT distinct u.id as user_id_event, ev.* /*u.created_at as user_created,*/
FROM
mixpanel.event ev
  left join mixpanel.people p on ev.distinct_id = p.distinct_id
  join aws_kinedu_app.users u on u.email = p.email
where ev.name ='PPPaymentSuccess'
and u.created_at >= '2019-07-26'


select alpha.properties, beta.*  from
TESTS.TestDAPVidas3 as alpha
inner join  TESTS.TestDAPVidas2 as beta
on alpha.user_id_event = beta.user_id




select *
from mixpanel.event
where name ='PPPaymentSuccess'
and time >='2019-07-26'



SELECT distinct user_id FROM `celtic-music-240111.TESTS.TestDAPVidas2`
where type='B'

SELECT alpha.*, beta.currency_code, beta.payment_status, beta.renewed_automatically , beta.sku, beta.usd_amount,
beta.store , beta.amount
FROM `celtic-music-240111.TESTS.TestDAPVidas2` as alpha
LEFT join `celtic-music-240111.aws_kinedu_app.sales` as beta on alpha.user_id = beta.user_id


SELECT distinct user_id
FROM `celtic-music-240111.TESTS.TestDAPVidas22`
where type='B'
and sku is not null
and payment_status ='paid'



SELECT sku,sum( usd_amount) as suma, count(*) as n  FROM `celtic-music-240111.TESTS.TestDAPVidas22`
where payment_status='paid'
and type='B'
group  by sku
order by sku

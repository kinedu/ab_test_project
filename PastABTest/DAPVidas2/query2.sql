SELECT distinct  ev.email as email_ev, ev.distinct_id,
case when properties like '%"Exp_Type":"B"%' then 'B'
     when properties like '%"Exp_Type":"A"%'  then 'A'
     end as type
FROM mixpanel.event ev
where ev.name ='AB_TEST'
and properties like '%DAP_Vidas(Jul26)%'

351392

SELECT distinct alpha.type,
CASE WHEN user_id is not null then user_id
     WHEN user_id is null then user_id2
END AS user_id,
CASE WHEN idioma_back is not null then idioma_back
     WHEN idioma_back is null then idioma_back2
end as idioma,
distinct_id as distinct_id_people_event,
case WHEN email is not null then email
     WHEN email is null then email_people
     end mail
FROM
(SELECT ev.*, u.id as user_id, u.locale as idioma_back,u.email,  p.email as email_people, u2.id as user_id2, u2.locale as idioma_back2
FROM `celtic-music-240111.TESTS.eventsDAP` as ev
left join aws_kinedu_app.users as u
on u.email = ev.email_ev
LEFT join  mixpanel.people as p
on p.distinct_id = ev.distinct_id
LEFT JOIN aws_kinedu_app.users as u2
on p.email = u2.email) as alpha
where alpha.user_id is not NULL





SELECT uDAP.*
FROM `celtic-music-240111.TESTS.usersDAP` as uDAP
inner join aws_kinedu_app.user_extra_data as ue
on uDAP.user_id = ue.user_id
where
ue.premium_conversion_date is not null
and  uDAP.type='A'


SELECT distinct alpha.*, beta.currency_code, beta.payment_status, beta.renewed_automatically , beta.sku, beta.usd_amount,
beta.store , beta.amount
FROM `celtic-music-240111.TESTS.usersDAP` as alpha
LEFT join `celtic-music-240111.aws_kinedu_app.sales` as beta on alpha.user_id = beta.user_id



select distinct email, properties, distinct_id,  event_id
from mixpanel.event
where name ='PPPaymentSuccess'
and time >='2019-07-26'


select * from
(select properties, event_id,
CASE WHEN user_mail is not null then 1
     when user_mail is null and distinct_id_people_event is not null then 1
     when user_mail is null and distinct_id_people_event is null then 0
end as filtro
from
(SELECT pagos.*, u.mail as user_mail, u2.distinct_id_people_event
FROM `celtic-music-240111.TESTS.pagos` as pagos
LEFT join TESTS.usersDAP  as u
on u.mail=pagos.email
left join TESTS.usersDAP as u2
on u2.distinct_id_people_event = pagos.distinct_id) )
where filtro =1


CON LA SALES DE BACK END


SELECT distinct uDAP.*, sales.id as sales_id, charge_response , charges.payment_source ,
case when payment_source in ( 'Email', 'INDAP', 'IAM', 'PUSH', 'PromoCode') then 1
     else  0
end as  promo
FROM `celtic-music-240111.TESTS.usersDAP` as uDAP
inner join aws_kinedu_app.sales as sales
on uDAP.user_id=sales.user_id
inner join ( select * from aws_kinedu_app.charges where false=false) as charges
on charges.sale_id = sales.id
where charges.payment_source is not null
order by  charges.payment_source

SELECT type, user_id, idioma, mail, payment_source, promo
FROM `celtic-music-240111.TESTS.salesBack`

SELECT type, user_id, idioma, mail, payment_source, promo
FROM `celtic-music-240111.TESTS.salesBack`
where type='B' and promo=1




SELECT ev.email, ev.distinct_id, ev.event_id, ev.time,  ev.os
FROM
mixpanel.event ev
where ev.name ='ActivityView'
and ev.time >= '2019-07-26'
order by  ev.event_id




select sub.time, sub.event_id, sub.os,
case WHEN user_id is not null then user_id
     when user_id is null then user_id2
end as user_id
FROM
(SELECT ac.*, uDAP.user_id, uDAP2.user_id as user_id2
FROM `celtic-music-240111.TESTS.activityViews` as ac
LEFT JOIN TESTS.usersDAP as uDAP
on uDAP.mail = ac.email
LEFT join TESTS.usersDAP as uDAP2
on uDAP2.distinct_id_people_event	= ac.distinct_id ) sub
where user_id is not null



SELECT   au.*, ue.premium_conversion_date, uDAP.type
FROM `celtic-music-240111.TESTS.actUsers` as au
left join ( select  *  from aws_kinedu_app.user_extra_data where id in
                (select min(id) from aws_kinedu_app.user_extra_data group by user_id) ) as ue
on ue.user_id = au.user_id
left join TESTS.usersDAP as uDAP
on uDAP.user_id = au.user_id




SELECT n_views, count(*) as n_users
from
(SELECT count(*) as n_views
FROM `celtic-music-240111.TESTS.actUsersdate`
where
premium_conversion_date is not null
and type='B'
and time <= premium_conversion_date
group by user_id)
group by n_views
order by n_views

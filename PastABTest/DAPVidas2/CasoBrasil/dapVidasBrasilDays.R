setwd("~/Desktop/Kinedu/Analytics/ABTest/ABTest/DAPVidas2/CasoBrasil")
dir()
data <- read.csv(file='dapVidasBrasilDay.csv', stringsAsFactors = FALSE)
library(dplyr)
library(lubridate)
names(data)
data$premium_conversion_date <- ymd_hms(data$premium_conversion_date)
data$user_inicio <- ymd_hms(data$user_inicio)
### vamos por las proporciones de conversion sin los premium de entrada
free <- subset(data,  is.na(premium_conversion_date) )
pre <- subset(data,  premium_conversion_date<= '2019-09-06' &  usd_amount_paid>0 &
                premium_conversion_date>'2019-07-24') 
datos <- rbind(free, pre)
table(datos$type)
names(datos)
x <- subset(datos, mp_country=='BR' )
#x <- datos
table(x$type)
table(x$status, x$type)
prop.test(c(111, 110), c( 17497, 17515))
x <- subset(x, !is.na(premium_conversion_date) )
table(x$source, x$type)
a <- 50
b <- 60
prop.test(c(a, b), c(a+61,b+50) )
##############info de test  y revenue
library(ggplot2)
ggplot(x, aes(usd_amount_paid, color = type)) +geom_density()
ks.test(x$usd_amount_paid[x$type=='A'], x$usd_amount_paid[x$type=='B'])
wilcox.test(x$usd_amount_paid[x$type=='A'], x$usd_amount_paid[x$type=='B'], conf.int = TRUE, 
            conf.level = .7)
mean(x$usd_amount_paid[x$type=='A'])
mean(x$usd_amount_paid[x$type=='B'])
##############################################
library(ggplot2)
index <- which(x$source=='NoCompra')
x$source[index] <- 'Producto'
y <- subset(x, type%in% c('A'))
ggplot(y, aes(usd_amount_paid, color = type)) +geom_density()
ks.test(y$usd_amount_paid[x$source=='Producto'], y$usd_amount_paid[x$source=='Promo'])
wilcox.test(y$usd_amount_paid[x$source=='Producto'], y$usd_amount_paid[x$source=='Promo'], conf.int = TRUE)
mean(y$usd_amount_paid[y$source=='Producto'])
mean(y$usd_amount_paid[y$source=='Promo'])




names(x)
s <-  x %>% select (sku, usd_amount_paid, type) %>% group_by(sku, type)%>%  summarise(amount =sum(usd_amount_paid), 
                                                                                      n=n()) %>% 
  arrange(sku)
write.csv(s, file='res.csv', row.names = FALSE) 
################################
y <- x %>% filter(sku != 'kinedu_premium_6_ht' )
index <- which(y$sku== 'kinedu_premium_1_599')
y$sku[index] <- 'Month'
index <- which(y$sku=='kinedu_premium_1_ht')
y$sku[index] <- 'Month'
table(y$sku)
index <- which(y$sku!='Month')
y$sku[index ] <- 'Year'
ks.test( y$usd_amount_paid[y$type=='A' & y$sku== 'Month'], y$usd_amount_paid[y$type=='B' & y$sku== 'Month'])
wilcox.test(y$usd_amount_paid[y$type=='A' & y$sku== 'Month'], y$usd_amount_paid[y$type=='B' & y$sku== 'Month'], conf.int = TRUE)
wilcox.test(y$usd_amount_paid[y$type=='A' & y$sku== 'Year'], y$usd_amount_paid[y$type=='B' & y$sku== 'Year'], conf.int = TRUE)
############### days until premium 
library(ggplot2)
ggplot(x, aes(days_until_premium, color = type)) +geom_density()
ks.test(x$days_until_premium[x$type=='A'], x$days_until_premium[x$type=='B'])
wilcox.test(x$days_until_premium[x$type=='A'], x$days_until_premium[x$type=='B'], conf.int = TRUE, 
            conf.level = .7)
mean(x$days_until_premium[x$type=='A'])
mean(x$days_until_premium[x$type=='B'])
y <- x %>% select (type, sku, days_until_premium)  %>% arrange(type, sku, days_until_premium )
write.csv(y, file='res.csv')
y$weeks <- cut(y$days_until_premium, breaks = c(0, 7, 14,  21,  521), include.lowest = TRUE)
table(y$type, y$weeks)
prop.test(c(31, 25) , c(111, 110 ))
  
  
  
  
  
#############

  SELECT
distinct
u.email, date_diff( ue.premium_conversion_date , u.signup_date , day) as days,
ue.premium_conversion_date,
cha.sku, cha.usd_amount_paid , cha.status,
u.signup_date,
case when cha.payment_source in ('Email', 'INDAP', 'IAM', 'PUSH', 'PromoCode')  then 'MKT'
     when cha.payment_source = 'SoftPaywall' then 'Paywall'
     else  'Producto'
     end
as payment_source,
case when ev.propertie like '%"Exp_Type":"A"%' then 'A'
else  'B'
end as type

FROM `celtic-music-240111.dbt_prod_caf.caf_mixpanel_event_abtests` as ev
inner join `celtic-music-240111.dbt_prod_caf.caf_mixpanel_people` as p
on p.distinct_id = ev.distinct_id
inner join dbt_prod_caf.caf_users as u
on p.email = u.email

inner join dbt_prod_caf.caf_user_extra_data as ue
on ue.user_id = u.user_id

left  join aws_kinedu_app.charges as cha
on cha.id = ue.premium_conversion_charge

where
 ev.propertie like '%IOS_AnchorTest(31Oct)%'
and u.os = 'Android'
and u.signup_date >= '2019-11-01'
order by u.signup_date



create or replace table TESTS.sianchor as (
SELECT distinct u.email
FROM `celtic-music-240111.mixpanel.event` as ev
inner join mixpanel.people as p
on p.distinct_id = ev.distinct_id
inner join aws_kinedu_app.users as u
on u.email=p.email
where ev.name='TAPCallToAction'
and ev.time >= '2019-11-01'
and ev.os='Android')




select * from TESTS.anchor_test2




create or replace table TESTS.si_anchor_sin_mkt as (
select *
from TESTS.anchor_test2 as base
where base.email in ( select email from TESTS.sianchor) )

create or replace table TESTS.anchor_test2 as (


  SELECT #ev.name, ev.time ,
distinct
u.email, date_diff( cast(ue.premium_conversion_date as date), cast(u.created_at as date) , day) as days,
ue.premium_conversion_date,
cha.sku, cha.usd_amount_paid , cha.status,
u.created_at,
case when cha.payment_source in ('Email', 'INDAP', 'IAM', 'PUSH', 'PromoCode')  then 'MKT'
     when cha.payment_source = 'SoftPaywall' then 'Paywall'
     else  'Producto'
     end
as payment_source,
case when properties like '%"Exp_Type":"A"%' then 'A'
else  'B'
end as type
FROM `celtic-music-240111.mixpanel.event` as ev
inner join `celtic-music-240111.mixpanel.people` as p
on p.distinct_id = ev.distinct_id
inner join aws_kinedu_app.users as u
on p.email = u.email
inner join aws_kinedu_app.user_extra_data as ue
on ue.user_id = u.id
left  join aws_kinedu_app.charges as cha
on cha.id = ue.premium_conversion_charge

where ev.name = 'AB_TEST'
and ev.properties like '%AOS_AnchorTest(31Oct)%'
and u.mp_os = 'Android'
 and u.created_at >= '2019-11-01'
order by u.created_at


)


create or replace table TESTS.sianchor as (
SELECT distinct u.email
FROM `celtic-music-240111.mixpanel.event` as ev
inner join mixpanel.people as p
on p.distinct_id = ev.distinct_id
inner join aws_kinedu_app.users as u
on u.email=p.email
where ev.name='TAPCallToAction'
and ev.time >= '2019-11-01'
and ev.os='Android')




select * from TESTS.anchor_test2




create or replace table TESTS.si_anchor_sin_mkt as (
select *
from TESTS.anchor_test2 as base
where base.email in ( select email from TESTS.sianchor) )

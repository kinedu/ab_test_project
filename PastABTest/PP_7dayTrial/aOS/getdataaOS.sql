CREATE OR REPLACE TABLE TESTS.aOS_7day_FreeTrial_v2  as ( /* nombre del test  */
  SELECT distinct u.email,
                  date_diff( ue.premium_conversion_date , u.signup_date , day) as days,
                  ue.premium_conversion_date,
                  cha.sku, cha.usd_amount_paid , cha.status,
                  u.signup_date,
  case when cha.payment_source in ('Email', 'INDAP', 'IAM', 'PUSH', 'PromoCode')  then 'MKT' /* source de pagos asociadas a las promociones de MKT */
       when cha.payment_source = 'SoftPaywall' then 'Paywall'
       else  'Producto'
       end
  as payment_source,
  case when ev.propertie like '%"Exp_Type":"A"%' then 'A'
  else  'B'
  end as type

  FROM `celtic-music-240111.dbt_prod_caf.caf_mixpanel_events` as ev
  inner join `celtic-music-240111.dbt_prod_caf.caf_mixpanel_people` as p
  on p.distinct_id = ev.distinct_id

  inner join dbt_prod_caf.caf_users as u
  on p.user_email = u.email

  inner join dbt_prod_caf.caf_user_extra_data as ue
  on ue.user_id = u.user_id

  left  join dbt_prod_caf.caf_charges as cha
  on cha.id = ue.premium_conversion_charge

  where ev.propertie like '%aOS_7day_FreeTrial_v2%'
   and u.signup_date >= '2019-12-23' /* fecha posterior a que se seteo el AB TEST en firebase */
  and u.email in (

                  SELECT u.email
                  FROM `dbt_prod_caf.caf_mixpanel_event_abtests` as ev /* subconjunto de eventos que son AB_TESTS */
                 inner join dbt_prod_caf.caf_mixpanel_people as p
                  on p.distinct_id = ev.distinct_id
                  inner join dbt_prod_caf.caf_users as u
                  on u.email = p.user_email
                  where  CAST( ev.time_real as date) >= '2019-12-23' /* fecha de seteo del AB_TEST */
                  AND ev.propertie like '%aOS_7day_FreeTrial_v2%' /* nombre del AB_TEST en firebase */
                  and ev.os in ('Android') /* sistema operativo */
               )
)

setwd("~/Desktop/Kinedu/Analytics/ABTest/SoftPaywall")
dir()
data <- read.csv(file='SoftPaywall.csv')
library(lubridate)
library(dplyr)
names(data)
data$premium_conversion_date <- ymd_hms(data$premium_conversion_date)
data$trial_start <- ymd_hms(data$trial_start)
data1 <- data %>% filter(is.na(trial_start))
data2 <- data %>% filter(trial_start <= '2019-09-26')
datos <- rbind(data1, data2)
free <- datos %>% filter(is.na(premium_conversion_date))
pre <- datos %>% filter(!is.na(premium_conversion_date))
pre <- pre %>% filter(premium_conversion_date >=  '2019-09-06')
datos <- rbind(free, pre)
table(datos$type)
datos <- datos%>% mutate(Premium = !is.na(premium_conversion_date))
table(datos$Premium, datos$type)
# test de proporciones para la conversion 
prop.test(x=c(75, 128), n= c(11427, 11575))
prop.test(c(43, 98), n=c(11427, 11575))
prop.test(c(32, 30), n=c(11427, 11575))

datos <- datos%>% mutate(trial=!is.na(trial_start) )
table(datos$trial, datos$type)
###########
prop.test(c(88, 171), n=c(11427, 11575))
a <- datos%>% filter(trial==TRUE ) %>% mutate(TrialConversion = premium_conversion_date>= trial_start )
table(a$TrialConversion, a$type)
prop.test(x = c(43, 98), n= c(88, 171))
library(ggplot2)
ggplot(datos, aes(  dias, color=type)) + geom_density() + theme_minimal()
datos %>% select( sku, usd_amount_paid, type   ) -> x
x %>% group_by(type, sku) %>% summarise( n =n())
prop.test(c(40, 100), n=c(75, 128))
table(datos$source , datos$type)
wilcox.test(datos$dias[ datos$type=='A'], datos$dias[ datos$type=='B'], conf.int = TRUE    )
mean(datos$dias[ datos$type=='A'], na.rm = TRUE)
mean(datos$dias[ datos$type=='B'], na.rm = TRUE    )

#
ggplot(datos, aes( usd_amount_paid , color=type)) + geom_density() + theme_minimal()
wilcox.test(datos$usd_amount_paid[ datos$type=='A'], datos$usd_amount_paid[ datos$type=='B'], 
            conf.int = TRUE)
mean(datos$usd_amount_paid[ datos$type=='A'], na.rm = TRUE)
mean(datos$usd_amount_paid[ datos$type=='B'], na.rm = TRUE) 
            
names(datos)


datos %>% filter(payment_source =='Paywall_Soft') %>%select (type, payment_source, sku) %>% group_by(type, payment_source) -> a



prop.test(c(0, 66) , n=c(66, 128))




##################

SELECT  distinct
         ue.premium_conversion_charge, ue.premium_conversion_date, ue.trial_charge,
         ue.trial_start, ue.user_id , cha.sku, cha.usd_amount_paid, cha.payment_source,
case when properties like '%"Exp_Type":"A"%' then 'A'
     when properties like '%"Exp_Type":"B"%' then 'B'
     end type,
case when payment_source in ('Email', 'INDAP', 'IAM', 'PUSH', 'PromoCode') then 'Mkt'
     when payment_source is null then payment_source
     else 'Producto' end source ,
     datetime_diff( cast(premium_conversion_date as datetime) , cast(trial_start as datetime), day) as dias

FROM `celtic-music-240111.mixpanel.event` as ev
inner join mixpanel.people as p
on p.distinct_id = ev.distinct_id
inner join aws_kinedu_app.users as u
on u.email = p.email

INNER JOIN ( SELECT * from `celtic-music-240111.aws_kinedu_app.user_extra_data` WHERE id IN (SELECT min(id)
                                                             FROM `celtic-music-240111.aws_kinedu_app.user_extra_data`
                                                             GROUP BY user_id ) ) ue on u.id = ue.user_id

left join aws_kinedu_app.charges as cha
on cha.id = ue.premium_conversion_charge


where ev.name='AB_TEST'
and ev.properties like  '%SoftPaywall_BR%'

############

# Descarga de datos para Android
SELECT  distinct
         ue.premium_conversion_charge, ue.premium_conversion_date, ue.trial_charge,
         ue.trial_start, ue.user_id , cha.sku, cha.usd_amount_paid, cha.payment_source,
case when properties like '%"Exp_Type":"A"%' then 'A'
     when properties like '%"Exp_Type":"B"%' then 'B'
     end type,
case when payment_source in ('Email', 'INDAP', 'IAM', 'PUSH', 'PromoCode') then 'Mkt'
     when payment_source is null then payment_source
     else 'Producto' end source ,
     datetime_diff( cast(premium_conversion_date as datetime) , cast(trial_start as datetime), day) as dias,
     u.mp_country

FROM `celtic-music-240111.mixpanel.event` as ev

inner join mixpanel.people as p
on p.distinct_id = ev.distinct_id

inner join aws_kinedu_app.users as u
on u.email = p.email

INNER JOIN ( SELECT * from `celtic-music-240111.aws_kinedu_app.user_extra_data` WHERE id IN (SELECT min(id)
                                                             FROM `celtic-music-240111.aws_kinedu_app.user_extra_data`
                                                             GROUP BY user_id ) ) ue on u.id = ue.user_id

left join aws_kinedu_app.charges as cha
on cha.id = ue.premium_conversion_charge


where ev.name='AB_TEST'
and ev.properties like  '%latam_prices_aos%'
and u.created_at >= '2019-09-10'
order by premium_conversion_date


################################################
# descarga de datos para iOS

{   "experiment_name": "",   "experiment_type": "",   "show_paywall": true,   "source": "Paywall_IA",   "test_type": "Control",   "body_bullets": {     "bullet_1_text": "Plan de actividades personalizado a sus necesidades.",     "bullet_2_text": "Acceso ilimitado al catálogo con 1600+ actividades.",     "bullet_3_text": "Reportes y gráficas de progreso y logros.",     "bullet_4_text": "Checklist de hitos del desarrollo para dar seguimiento.",     "bullet_5_text": "Identifica las áreas que necesita apoyo y cómo ayudar.",     "bullet_6_text": "Acceso a +450 artículos creados por especialistas.",     "bullet_7_text": "Invita a familiares o niñera a tu cuenta.",     "bullet_8_text": "Agrega hasta 5 bebés."   },   "discount_info": {     "first_text": "OFF",     "first_text_Color": "#D0021B",     "second_text": "y obtén acceso a:",     "second_text_color": "#4A4A4A",     "discount_text": "40%",     "discount_text_color": "#D0021B"   },   "hero_images": {     "iphone_image_url": "https://firebasestorage.googleapis.com/v0/b/kinedumobile.appspot.com/o/ia_paywall%2Fhero_ES_G_iphone1.png?alt=media&token=7cf479cc-d7c7-4ae2-908d-0eb6b2d46cd0",     "ipad_portrait_image_url": "https://firebasestorage.googleapis.com/v0/b/kinedumobile.appspot.com/o/ia_paywall%2Fhero_G_ES_Ipad_P.png?alt=media&token=50e78339-1f40-40b4-b8ac-9fe39d47fffd",     "ipad_landscape_image_url": "https://firebasestorage.googleapis.com/v0/b/kinedumobile.appspot.com/o/ia_paywall%2Fhero_G_ES_ipad_L.png?alt=media&token=4a675783-d84c-47dc-a3eb-0ade2daec6ef"   },   "pricing_info": {     "original_price_sku": "kinedu_premium_12_6999",     "original_price_text": "Precio original:",     "original_price_text_after_sku": "",     "primary_Button_text": "Prueba 7 días GRATIS",     "promo_sku": "kinedu_premium_12_3999_ft_7",     "promo_text": "Con descuento:",     "promo_text_after_sku": "/ año",     "secondary_Button_text": "No me interesa esta oferta"   } }



#################

setwd("~/Desktop/Kinedu/Analytics/ABTest/LowPricesLatam/")
dir()
data <- read.csv(file='LowPricesLatamiOS.csv')
library(lubridate)
library(dplyr)
names(data)
data$premium_conversion_date <- ymd_hms(data$premium_conversion_date)
data$trial_start <- ymd_hms(data$trial_start)
data1 <- data %>% filter(is.na(trial_start))
data2 <- data %>% filter(trial_start <= '2019-10-01')
datos <- rbind(data1, data2)
free <- datos %>% filter(is.na(premium_conversion_date))
pre <- datos %>% filter(!is.na(premium_conversion_date))
pre <- pre %>% filter(premium_conversion_date >=  '2019-09-10')
datos <- rbind(free, pre)
table(datos$type)
datos <- datos%>% mutate(Premium = !is.na(premium_conversion_date))
table(datos$Premium, datos$type)
# test de proporciones para la conversion 
prop.test(x=c(57, 47), n= c(3357, 3344  ))

datos <- datos%>% mutate(trial=!is.na(trial_start) )
table(datos$trial, datos$type)
prop.test(c(41, 32), n= c(3357, 3344))

###########
prop.test(c(46, 36), c(57, 47))
a <- datos%>% filter(trial==TRUE ) %>% mutate(TrialConversion = premium_conversion_date>= trial_start )
table(a$TrialConversion, a$type)
#
x <- datos%>% filter(trial==TRUE ) %>% mutate(TrialConversion = !is.na(premium_conversion_date) )
table(x$TrialConversion, x$type)

prop.test(x = c(1, 5), n= c(61, 73))
library(ggplot2)
ggplot(datos, aes(  dias, color=type)) + geom_density() + theme_minimal()
datos %>% select( sku, usd_amount_paid, type   ) -> x
x %>% group_by(type, sku) %>% summarise( n =n())
prop.test(c(33, 48), n=c(61, 73))
table(datos$source , datos$type)

wilcox.test(datos$dias[ datos$type=='A'], datos$dias[ datos$type=='B'], conf.int = TRUE    )
mean(datos$dias[ datos$type=='A'], na.rm = TRUE)
mean(datos$dias[ datos$type=='B'], na.rm = TRUE    )
table(datos$payment_source, datos$type)
table(datos$source, datos$type)
prop.test(c(37, 25), c(57, 47))

#


ggplot(datos, aes( usd_amount_paid , color=type)) + geom_density() + theme_minimal()
wilcox.test(datos$usd_amount_paid[ datos$type=='A'], datos$usd_amount_paid[ datos$type=='B'], 
            conf.int = TRUE)
mean(datos$usd_amount_paid[ datos$type=='A'], na.rm = TRUE)
mean(datos$usd_amount_paid[ datos$type=='B'], na.rm = TRUE) 












##################

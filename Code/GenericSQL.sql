CREATE OR REPLACE TABLE TESTS.<nombre del test>  as ( /* nombre del test  */
SELECT distinct u.email,
               date_diff( ue.premium_conversion_date , u.signup_date , day) as days,
               ue.premium_conversion_date,
               cha.sku, cha.usd_amount_paid , cha.status,
               u.signup_date,
case when cha.payment_source in ('Email', 'INDAP', 'IAM', 'PUSH', 'PromoCode')  then 'MKT'
                            /* source de pagos asociadas a las promociones de MKT */
    when cha.payment_source = 'SoftPaywall' then 'Paywall'
    else  'Producto'
    end
as payment_source,
CASE WHEN ev.propertie like '%"Exp_Type":"a_promo"%' THEN 'A' #propiedades del evento que permiten diferenciar el estrato, Varian de test a test
WHEN ev.propertie like '%"Exp_Type":"b_promo"%' THEN 'B'
WHEN ev.propertie like '%"Exp_Type":"c_promo"%' THEN 'C'
END AS type
FROM `celtic-music-240111.dbt_prod_caf.caf_mixpanel_events` as ev

inner join `celtic-music-240111.dbt_prod_caf.caf_mixpanel_people` as p
on p.distinct_id = ev.distinct_id

inner join dbt_prod_caf.caf_users as u
on p.user_email = u.email

inner join dbt_prod_caf.caf_user_extra_data as ue
on ue.user_id = u.user_id

left  join dbt_prod_caf.caf_charges as cha
on cha.id = ue.premium_conversion_charge

where ev.propertie like '%<nombre del abtest en firebase>%'
and u.signup_date >= '<fecha de seteo en firebase>'  /* fecha posterior a que se seteo el AB TEST en firebase CUIDAMOS QUE SEA USUARIO NUEVO EN BACKEND*/
and u.email in (

               SELECT u.email
               FROM `dbt_prod_caf.caf_mixpanel_event_abtests` as ev
                                      /* subconjunto de eventos que son AB_TESTS */
              inner join dbt_prod_caf.caf_mixpanel_people as p
               on p.distinct_id = ev.distinct_id
               inner join dbt_prod_caf.caf_users as u
               on u.email = p.user_email
               where  timestamp_millis(ev.time_real) >= '<fecha de seteo en firebase>' /* cuidamos que sea un evento lanzado despues de la fecha de seteo */
/* fecha de seteo del AB_TEST, aunmentamos un día para no tener conflicto con las zonas horarias */
               AND ev.propertie like  '%<nombre del abtest en firebase>%'
                                                /* nombre del AB_TEST en firebase */
     and ev.os in ('<OS>')
               /* sistema operativo */
            )
)
